// *****************************************************************************
// commonFunctions.cpp
//
// This file contains the implementation of functions common to all examples.
//
// ****************************************************************************
// Except as specified in the OpenAccess terms of use of Cadence or Silicon
// Integration Initiative, this material may not be copied, modified,
// re-published, uploaded, executed, or distributed in any way, in any medium,
// in whole or in part, without prior written permission from Cadence.
//
//                Copyright 2002-2005 Cadence Design Systems, Inc.
//                           All Rights Reserved.
//
// To distribute any derivative work based upon this file you must first contact
// Si2 @ contracts@si2.org.
//
// *****************************************************************************
// *****************************************************************************


#include "commonFunctions.h"



// *****************************************************************************
// handleFBCError
//
// This function reads the feature list from an oaCompatibility error and 
// formats an easy to read exception message.
// *****************************************************************************
void
handleFBCError(oaCompatibilityError &err)
{
    const oaFeatureArray    features = err.getFeatures();
    oaString                unsupported;
    static const oaString   domainFmt("\t%s of the %s feature, which applies "
                                      "to %s objects in the %s portion of the "
                                      "%s database in the %s domain.\n");
    static const oaString   noDomainFmt("\t%s of the %s feature, which applies "
                                        "to %s objects in the %s portion of "
                                        "the %s database.\n");

    static const oaString   errorMsg("An incompatible OpenAccess database, %s, was "
                                     "encountered.\nException from OA: %s\nThis "
                                     "database contains the following unsupported "
                                     "features:\n%s");

    for (oaUInt4 i = 0; i < features.getNumElements(); i++) {
        oaString    tmp;
        oaString    name;

        oaFeature   *feature = features[i];
        feature->getName(name);

        if (feature->getDomain() == oacNoDomain) {
            tmp.format((const char*) noDomainFmt,
                       (const char*) feature->getDataModelModType().getName(),
                       (const char*) name,
                       (const char*) feature->getObjectType().getName(),
                       (const char*) feature->getCategory().getName(),
                       (const char*) feature->getDBType().getName());
        } else {
            tmp.format((const char*) domainFmt,
                       (const char*) feature->getDataModelModType().getName(),
                       (const char*) name,
                       (const char*) feature->getObjectType().getName(),
                       (const char*) feature->getCategory().getName(),
                       (const char*) feature->getDBType().getName(),
                       (const char*) feature->getDomain().getName());
        }

        unsupported += tmp;
    }

    oaString                output;
    output.format(errorMsg,
                  (const char*) err.getDatabaseName(),
                  (const char*) err.getMsg(),
                  (const char*) unsupported);
    cout << output << endl;
}



// ****************************************************************************
// updateLibDefsFile
//  
//  This function will add a new entry in the current lib.defs file.
// ****************************************************************************
void
updateLibDefsFile(const oaScalarName    &libraryName,
                  const oaString        &libraryPath)
{
    // Save the new library definition in the session top library definition
    // the. Since oaLibdefList::openLibs() opened in 'r'ead-only mode,
    // first call get() on top list to change its mode to 'a'ppend.
    oaLibDefList *list = oaLibDefList::getTopList();

    // If no lib.defs file exists, create a new one.
    if (!list) {
        list = oaLibDefList::get("lib.defs", 'w');
        list->save();
        oaLibDefList::openLibs();
    }

    // Now add the library definition to the file. If no file exists at this
    // point, then none could be found and created on disk.
    if (list) {
        oaString    topListPath;
        list->getPath(topListPath);
        list->get(topListPath, 'a');
        oaLibDef    *newLibDef = oaLibDef::create(list, libraryName, libraryPath);
        list->save();
    } else {
        cerr << "ERROR: Unable to append to lib.defs file." << endl;
        exit(1);
    }
}



// *****************************************************************************
// openLibrary
//
// This function returns a pointer to an OpenAccess library. If the library
// exists on disk, it is opened. If it does not exist, it is created. An entry
// to the lib.defs file is added if necessary.
//
// The OpenAccess coding examples support the environment variable $DMSystem.
// It can be used to choose the DM system used when creating a new library.
// *****************************************************************************
oaLib *
openLibrary(const oaScalarName  &libName,
            const oaString      &libPath)
{
    oaLib *lib = oaLib::find(libName);

    if (!lib) {
        if (oaLib::exists(libPath)) {

            // Library does exist at this path but was not in lib.defs
            lib = oaLib::open(libName, libPath);

        } else {

            // Library does not exist on disk, so create it.
            char *DMSystem = getenv("DMSystem");
            if (DMSystem) {
                lib = oaLib::create(libName, libPath, oacSharedLibMode, DMSystem);
            } else {
                lib = oaLib::create(libName, libPath);
            }
        }

        if (lib) {

            // We need to update the user's lib.def file since we either
            // found or created the library without a lib.defs reference.
            updateLibDefsFile(libName, libPath);

        } else {

            // Print error mesage 
            cerr << "ERROR: Unable to create library at path " << libPath;
            cerr << "." << endl;
            exit(1);

        }
    }

    return lib;
}



// *****************************************************************************
// closeOpenDesigns
//
// This function closes all open designs in the session.
// *****************************************************************************
void
closeOpenDesigns()
{
    oaIter<oaDesign> designs(oaDesign::getOpenDesigns());
    
    while (oaDesign *design = designs.getNext()) {
        design->close();
    }
}
