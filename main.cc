#pragma GCC diagnostic ignored "-Wwrite-strings"
#include <iostream>
#include <string>
#include "lef_def_parser.h"
using namespace std;

int main(int argc, char *argv[]){
try {
        // Initialize OA with data model 3, since incremental technology
        // databases are supported by this application.
    char defkey[]="-def";
    char lefkey[]="-lef";
    char lmapkey[]="-lmap"; 
	char *type;
    for(int i=1;i<argc;i++){
	if(strcmp(argv[i],defkey)==0){
	    cout<<"parsing def_lib\n";
	    if(argc>i+4){
			type="layout";
 	   	    OA_stdCell test1(argv[i+3],argv[i+2],argv[i+1],type);
		    test1.getInstBoundary(argv[i+4]);
		    i+=4;
	    }
	    else{
		    cout<<"not enough argument\n";
		    cout<<"command -def [libPath] [libName] [cellName] [outFile]\n";
		    exit(-1);
	    }	    
    	}
	else if(strcmp(argv[i],lefkey)==0){
	    cout<<"parsing lef_lib\n";
	    if(argc>i+3){
			type="abstract";
 	   	    OA_stdCell test1(NULL,argv[i+2],argv[i+1],type);
       		test1.getLayerMap();
			ofstream ofs;
			string filename = string() + argv[i + 3] + "/result.json";
			ofs.open(filename.c_str());
			if(ofs.is_open() == false){
				cerr << "Fail to open " << filename << " for writing" << endl;
				exit(-1);
			}
			ofs << "[" << endl;
       		test1.getAllCell(argv[i+2],argv[i+1],type,argv[i+3], ofs);
			ofs << "]" << endl;
			ofs.close();
		    i+=3;
	    }
	    else{
		    cout<<"not enough argument\n";
		    cout<<"command -lef [libPath] [libName] [cellName] [outPath]\n";
		    exit(-1);
	    }
	}
    }
    } catch (oaCompatibilityError &ex) {
        handleFBCError(ex);
        exit(1);

    } catch (oaException &excp) {
        cout << "ERROR: " << excp.getMsg() << endl;
        exit(1);
    }
    return 0;
}
