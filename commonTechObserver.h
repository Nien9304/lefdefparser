// *****************************************************************************
// commonTechObserver.cpp
//
// This file contains the implementation of an oaTech observer for technology
// conflict notifications.
//
// This code is used in all examples to alert the users of technology conflicts.
//
// ****************************************************************************
// Except as specified in the OpenAccess terms of use of Cadence or Silicon
// Integration Initiative, this material may not be copied, modified,
// re-published, uploaded, executed, or distributed in any way, in any medium,
// in whole or in part, without prior written permission from Cadence.
//
//                Copyright 2002-2005 Cadence Design Systems, Inc.
//                           All Rights Reserved.
//
// To distribute any derivative work based upon this file you must first contact
// Si2 @ contracts@si2.org.
//
// *****************************************************************************
// *****************************************************************************
// *****************************************************************************
// opnTechConflictObserver
//
// This class handles notifications of technology conflicts.
// *****************************************************************************



#ifndef commonTechObserver_P
#define commonTechObserver_P


#include <iostream>
#include "oaDesignDB.h"

using namespace oa;
using namespace std;



// *****************************************************************************
// opnTechConflictObserver
//
// This class handles technology conflict notifications and outputs a detailed
// error message.
// *****************************************************************************
class opnTechConflictObserver : public oaObserver<oaTech> {
public:
                            opnTechConflictObserver(oaUInt4 priorityIn);

    virtual void            onConflict(oaTech                   *mostDerivedTech,
                                       oaTechConflictTypeEnum   conflictType,
                                       oaObjectArray            conflictingObjs);

    virtual void            onClearanceMeasureConflict(oaTechArray cTechs);

    virtual void            onDefaultManufacturingGridConflict
        (oaTechArray cTechs);

    virtual void            onGateGroundedConflict(oaTechArray cTechs);
    virtual void            onDBUPerUUConflict(oaTechArray  cTechs,
                                               oaViewType   *viewType);

    virtual void            onUserUnitsConflict(oaTechArray cTechs,
                                                oaViewType  *viewType);

    virtual void            onProcessFamilyConflict(const oaTechArray &cTechs);

    virtual void            onExcludedLayerConflict(oaTech                  *mostDerivedTech,
                                                    const oaPhysicalLayer   *l1,
                                                    const oaLayer           *l2);

private:
    void                    getObjectLibName(const oaObject *obj,
                                             oaString       &str) const;

    void                    printErrorMsg(const char    *type,
                                          const char    *elements,
                                          const char    *list) const;

    static oaNativeNS       ns;
    static oaString         format;
};



#endif
