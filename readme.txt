###############################################
#         OA API ENVIRONMENT SETTING          #
# in macro.def OPTMODE dont use dbg ,but opt  #
#     before run program source oa.source     #
###############################################
ENVIRONMENT SETTING:
change common library's path in macro.def
	& lef_def_parser include headerfile

COMMAND LIST:
lefdef2oa:
	lef2oa -lef [techfile.lef] -lib [yourlibname] -overwrite
	lef2oa -lef [reffile.lef] -lib [yourlibname] -techLib [techLib] -overwrite
	def2oa -def [deffile.def] -lib [yourlibname] -techLib [techLib] -refLib [refLib] -overwrite
oa2json:
	./main -def [libPath] [libName] [cellName] [outFile]
		example:  ./main -def ./CORE_lib CORE_lib CORE testlib/CORE.json
	./main -lef [libPath] [libName] [cellName] [outPath]
		example:  ./main -lef ./l90_lef_lib l90_lef_lib  testlib/
