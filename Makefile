include macro.defs

PROG = main
SORC = lef_def_parser
TARGET = ../oa2json
COMMON1 = commonTechObserver
COMMON2 = commonLibDefListObserver
COMMON3 = commonFunctions
DEFAULT: $(TARGET)


# Compile the application code
$(SORC).o: $(SORC).cpp
	$(CCPATH) $(CXXOPTS) $(DEBUG) -o $(SORC).o -I $(TOOLSDIR)/include/oa \
	 -I $(TOOLSDIR)/include \
	 -c $(SORC).cpp


$(PROG).o: $(PROG).cc
	$(CCPATH) $(CXXOPTS) $(DEBUG) -o $(PROG).o -I $(TOOLSDIR)/include/oa \
	 -I $(TOOLSDIR)/include \
	 -c $(PROG).cc
$(COMMON1).o: $(COMMON1).cpp $(COMMON1).h
	$(CCPATH) $(CXXOPTS) $(DEBUG) -o $(COMMON1).o -I $(TOOLSDIR)/include/oa \
	 -I $(TOOLSDIR)/include \
	 -c $(COMMON1).cpp 

$(COMMON2).o: $(COMMON2).cpp $(COMMON2).h
	$(CCPATH) $(CXXOPTS) $(DEBUG) -o $(COMMON2).o -I $(TOOLSDIR)/include/oa \
	 -I $(TOOLSDIR)/include \
	 -c $(COMMON2).cpp 

$(COMMON3).o: $(COMMON3).cpp $(COMMON3).h
	$(CCPATH) $(CXXOPTS) $(DEBUG) -o $(COMMON3).o -I $(TOOLSDIR)/include/oa \
	 -I $(TOOLSDIR)/include \
	 -c $(COMMON3).cpp 


# Link the executabl
$(TARGET): $(PROG).o $(SORC).o $(COMMON1).o $(COMMON2).o $(COMMON3).o $(OA_LIB_LIST) 
	$(CCPATH) $(CXXOPTS) -o $(TARGET)  $(PROG).o $(SORC).o \
	$(COMMON1).o $(COMMON2).o $(COMMON3).o \
	 -L$(OA_LIB_DIR) \
	 -loaCommon \
	 -loaBase \
	 -loaPlugIn\
	 -loaDM\
	 -loaTech\
         -loaDesign\
	 $(SYSLIBS)

clean:
	@/bin/rm  -rf $(PROG).o $(SORC).o
	@/bin/rm  -rf $(TARGET)
	@/bin/rm  -rf $(COMMON1).o 
	@/bin/rm  -rf $(COMMON2).o 
	@/bin/rm  -rf $(COMMON3).o 

