// *****************************************************************************
// commonLibDefListObserver.h
//
// This is the header file for the opnLibDefListObserver class.
//
// ****************************************************************************
// Except as specified in the OpenAccess terms of use of Cadence or Silicon
// Integration Initiative, this material may not be copied, modified,
// re-published, uploaded, executed, or distributed in any way, in any medium,
// in whole or in part, without prior written permission from Cadence.
//
//                Copyright 2002-2005 Cadence Design Systems, Inc.
//                           All Rights Reserved.
//
// To distribute any derivative work based upon this file you must first contact
// Si2 @ contracts@si2.org.
//
// *****************************************************************************
// *****************************************************************************



#ifndef commonLibDefListObserver_P
#define commonLibDefListObserver_P

#include <iostream>
#include "oaDesignDB.h"

using namespace oa;
using namespace std;



// *****************************************************************************
// opnLibDefListObserver
//
// This class receives notifications regarding the library definitions file.
// *****************************************************************************
class opnLibDefListObserver : public oaObserver<oaLibDefList> {
public:
                            opnLibDefListObserver(oaUInt4   priorityIn,
                                                  oaBoolean enabledIn = true);

    oaBoolean               onLoadWarnings(oaLibDefList                 *obj,
                                           const oaString               &msg,
                                           oaLibDefListWarningTypeEnum  type);
};



#endif
