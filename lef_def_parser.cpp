#include "lef_def_parser.h"
using namespace oa;
using namespace std;

//static oaNativeNS ns;

OA_stdCell::OA_stdCell(char *designName,char *d_libName,char *d_libPath,char *viewType)
{
   // try {
        // Initialize OA with data model 3, since incremental technology
        // databases are supported by this application.

	oaDesignInit(oacAPIMajorRevNumber, oacAPIMinorRevNumber, 4);

        oaString                libPath(d_libPath);
        oaString                library(d_libName);
        oaString                view(viewType);

        oaScalarName            libName(ns,
                                        library);

        oaScalarName            viewName(ns,
                                         view);

        // Setup an instance of the oaTech conflict observer.
        opnTechConflictObserver myTechConflictObserver(1);
        // Setup an instance of the oaLibDefList observer.
        opnLibDefListObserver   myLibDefListObserver(1);
        // Read in the lib.defs file.
        oaLibDefList::openLibs();

        // Try to get a pointer to the library. If it does not exist yet,
        // it will be created and added to the lib.defs file.
        m_lib = openLibrary(libName, libPath);

        // Create the design with the specified viewType,
        // Opening it for a 'write' operation.
//        oaViewType              *vt = oaViewType::get(oacMaskLayout);
//        oaDesign                *design = oaDesign::open(libName, cellName, viewName, vt, 'r');
//
	if(designName!=NULL){//def
       		oaString	cell(designName);
	        oaScalarName	cellName(ns,cell);
	        m_design = oaDesign::open(libName, cellName, viewName, 'r');
	}
/*
    } catch (oaCompatibilityError &ex) {
        handleFBCError(ex);
        exit(1);

    } catch (oaException &excp) {
        cout << "ERROR: " << excp.getMsg() << endl;
        exit(1);
    }
*/

}
OA_stdCell::~OA_stdCell()
{
    m_lib->close();
}
void OA_stdCell::getLayerMap(){

	oaString 	layername; 
	m_tech = oaTech::open(m_lib);
	oaCollection<oaLayer, oaTech> m_layer(m_tech->getLayers());
	oaIter<oaLayer> layerIter(m_tech->getLayers());
	while (	oaLayer * tmp_layer= layerIter.getNext()) {

		tmp_layer->getName(layername);
		m_layermap[tmp_layer->getNumber()]=layername;
	}
	

}
void OA_stdCell::getAllCell(char *d_libName,char *libPath,char *viewType,char *outPath, ostream &result_file){
        oaString        library(d_libName);
        oaString        view(viewType);
	oaString 	cell;
        oaScalarName            libName(ns,library);
        oaScalarName            viewName(ns,view);
	char outFile[80];
	bool flag = false;
	m_lib->getAccess(oacWriteLibAccess);
	oaCollection<oaCell, oaLib> cell_collection(m_lib->getCells());
	cout<<"Total "<<cell_collection.getCount()<<" cells\n";
	oaIter<oaCell> cellIter(m_lib->getCells());
	while (oaCell * tmp_cell = cellIter.getNext()) {
		tmp_cell->getName(ns,cell);
		if(flag) result_file << ",";
		result_file << "\"" << cell << "\"" << endl;
		strcpy(outFile,outPath);
		strcat(outFile,"//");
		strcat(outFile,cell);
		strcat(outFile,".json");
		fout.open(outFile,std::fstream::out);
	        oaScalarName	cellName(ns,cell);
		m_design = oaDesign::open(libName,cellName,viewName, 'r');
		getCellInf();	
		fout.close();
	  	m_design->close();
		flag = true;
	}
}
void OA_stdCell::getInstBoundary(char *outPath)
{
    // Get the TopBlock of the current design
    oaBlock *block = m_design->getTopBlock();
    if (block) {
	oaString	instName;
	oaString	cellName;
        oaString        netName;
        oaString        termName;
	oaPointArray	PointArray;
	oaSnapBoundary  *Boundary;
	oaBlock 	*tmp_Block;
	oaPoint		tmp_point;
	oaBox		Box;
	bool is_first=true;
        fout.open(outPath,std::fstream::out);
	fout<<"["<<endl;

	oaCollection<oaInst, oaBlock> cell_collection(block->getInsts());
	oaIter<oaInst>   InstIterator(block->getInsts());

	while(oaInst *tmp_Inst=InstIterator.getNext()){
		if(!is_first)
			fout<<"},\n";
		is_first=false;

//		tmp_Inst->getOrigin(tmp_point);

		tmp_Inst->getBBox(Box);
		tmp_Inst->getName(ns,instName);
		tmp_Inst->getCellName(ns,cellName);
		fout<<"{\"name\":\""<<instName<<"\",";
		fout<<"\"cell\":\""<<cellName<<"\",";
		fout<<"\"location\":["<<Box.left()<<","<<Box.right()<<","<<Box.bottom()<<","<<Box.top()<<"],";
//		fout<<"\"location\":["<<tmp_point.x()<<" "<<tmp_point.y()<<"],";
		if(tmp_Inst->getOrient().getName()=="R0"){
				fout<<"\"orient\":[false,false]";
		}
		else if(tmp_Inst->getOrient().getName()=="MX")
		{
				fout<<"\"orient\":[true,false]";
		}
		else if(tmp_Inst->getOrient().getName()=="MY")
		{
				fout<<"\"orient\":[false,true]";
		}
		else if(tmp_Inst->getOrient().getName()=="R180")
		{
				fout<<"\"orient\":[true,true]";
		}
		else
		{
				fout<<"\"orient\":[null,null]";
		}
	}
	fout<<"}\n]";
	fout.close();
    } else {
        cout << "There is no block in this design" << endl;
    }
}
void OA_stdCell::getCellInf()
{
    bool is_first;
    bool is_first2;
    // Get the TopBlock of the current design
    oaBlock *block = m_design->getTopBlock();
    pin_list_map.clear();
    if (block) {
        oaString        netName;
        oaString        termName;
	oaBox		Box;
//	oaPoint		Point;
	oaPointArray	PointArray;
	oaSnapBoundary  *Boundary;
//	block->getBBox(Box);
	Boundary=oaSnapBoundary::find(block);
	if(Boundary!=NULL){
		Boundary->getBBox(Box);
	}
/*
	oaCollection<oaBlockage, oaBlock> blockage_collection(block->getBlockages());
	oaIter<oaBlockage>  blockageIter(block->getBlockages());
	while (oaBlockage *tmp_blockage = blockageIter.getNext()) {
		tmp_blockage->getPoints(PointArray);
		cout<<PointArray.getArea()<<endl;
		
	}
			Point=Box.lowerLeft();
			fout<<"{\"boundary\":["<<Point.x()<<","<<Point.y();
			Point=Box.lowerLeft();
			fout<<","<<Point.x()<<","<<Point.y()<<"],\n";
*/
	fout<<"{\"boundary\":["<<Box.left()<<","<<Box.right()<<","<<Box.bottom()<<","<<Box.top()<<"],\n";
	fout<<"\"pin_list\":[";
        oaIter<oaPin>   pinIterator(block->getPins());
	is_first2=true;
	pins *tmp_pin;
	pin_list *tmp_pin_list;
	while (oaPin *pin = pinIterator.getNext()) {
		oaCollection<oaPinFig, oaPin> fig_collection(pin->getFigs());
		pin->getTerm()->getName(ns,netName);
	        oaIter<oaPinFig>  figIter(pin->getFigs());
		is_first=true;
        	while (oaPinFig * fig = figIter.getNext()) {
			tmp_pin=new pins;	
        	        fig->getBBox(Box);
			oaShape* tmp_shape=static_cast<oaShape*>(fig);
			tmp_pin->layer=m_layermap[tmp_shape->getLayerNum()];

			tmp_pin->left_bound=Box.left();
			tmp_pin->right_bound=Box.right();
			tmp_pin->down_bound=Box.bottom();
			tmp_pin->up_bound=Box.top();			

			std::map<string,pin_list*>::iterator search=pin_list_map.find(string(netName));

			if(search!=pin_list_map.end())
				search->second->push_back(tmp_pin);
			else {
				tmp_pin_list = new pin_list;
				tmp_pin_list->clear();
				tmp_pin_list->push_back(tmp_pin);
				pin_list_map.insert(std::pair<string,pin_list*>(string(netName),tmp_pin_list));
			}
	        }

	}
	for(std::map<string,pin_list*>::iterator it=pin_list_map.begin(); it!=pin_list_map.end(); ++it){
		if(!is_first2)
			fout<<"},\n";
		is_first2=false;
			
		fout<<"{\"name\":\""<<it->first<<"\",\"rect_list\":["<<endl;
		is_first=true;
		for(vector<pins*>::iterator it1 = it->second->begin() ; it1 != it->second->end(); ++it1){
			if(!is_first)
				fout<<"},\n";
			is_first=false;
			fout<<"{\"layer\":\""<<(*it1)->layer<<"\",";
			fout<<"\"boundary\":["<<(*it1)->left_bound<<","<<(*it1)->right_bound<<","<<(*it1)->down_bound<<","<<(*it1)->up_bound<<"]";	
		}
		fout<<"}\n";
		fout<<"]";
	}
/*

     	while (oaPin *pin = pinIterator.getNext()) {
		if(!is_first2)
			fout<<"},\n";
		is_first2=false;
			oaCollection<oaPinFig, oaPin> fig_collection(pin->getFigs());
		pin->getTerm()->getName(ns,netName);
			
		fout<<"{\"name\":\""<<netName<<"\",\"rect_list\":["<<endl;

	        oaIter<oaPinFig>  figIter(pin->getFigs());
		is_first=true;
        	while (oaPinFig * fig = figIter.getNext()) {
        	        fig->getBBox(Box);
			oaShape* tmp_shape=static_cast<oaShape*>(fig);

			if(!is_first)
				fout<<"},\n";
			is_first=false;
			fout<<"{\"layer\":\""<<m_layermap[tmp_shape->getLayerNum()]<<"\",";
			fout<<"\"boundary\":["<<Box.left()<<","<<Box.right()<<","<<Box.bottom()<<","<<Box.top()<<"]";			
	        }
		fout<<"}\n";
		fout<<"]";
	}*/
	fout<<"}\n]\n}";
    } else {
        cout << "There is no block in this design" << endl;
    }

}
