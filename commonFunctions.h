// *****************************************************************************
// commonFunctions.h
//
// This file contains the prototypes of functions common to all examples.
//
// ****************************************************************************
// Except as specified in the OpenAccess terms of use of Cadence or Silicon
// Integration Initiative, this material may not be copied, modified,
// re-published, uploaded, executed, or distributed in any way, in any medium,
// in whole or in part, without prior written permission from Cadence.
//
//                Copyright 2002-2005 Cadence Design Systems, Inc.
//                           All Rights Reserved.
//
// To distribute any derivative work based upon this file you must first contact
// Si2 @ contracts@si2.org.
//
// *****************************************************************************
// *****************************************************************************



#ifndef commonFunctions_P
#define commonFunctions_P

#include <iostream>
#include "oaDesignDB.h"

using namespace oa;
using namespace std;



// ******************************************************************************
// This function reads the feature list from an oaCompatibility error and formats
// an easy to read exception message.
// ******************************************************************************
void
        handleFBCError(oaCompatibilityError &err);



// ******************************************************************************
//  This function will add a new entry in the current lib.defs file.
// ******************************************************************************
void
        updateLibDefsFile(const oaScalarName    &libraryName,
                          const oaString        &libraryPath);



// ******************************************************************************
// This function returns a pointer to an OpenAccess library. If the library
// exists on disk, it is opened. If it does not exist, it is created. An entry
// to the lib.defs file is added if necessary.
// ******************************************************************************
oaLib   *
    openLibrary(const oaScalarName  &libName,
                const oaString      &libPath);



// *****************************************************************************
// closeOpenDesigns
//
// This function closes all open designs in the session.
// *****************************************************************************
void
closeOpenDesigns();


#endif
