// *****************************************************************************
// commonLibDefListObserver.cpp
//
//
// ****************************************************************************
// Except as specified in the OpenAccess terms of use of Cadence or Silicon
// Integration Initiative, this material may not be copied, modified,
// re-published, uploaded, executed, or distributed in any way, in any medium,
// in whole or in part, without prior written permission from Cadence.
//
//                Copyright 2002-2005 Cadence Design Systems, Inc.
//                           All Rights Reserved.
//
// To distribute any derivative work based upon this file you must first contact
// Si2 @ contracts@si2.org.
//
// *****************************************************************************
// *****************************************************************************


#include "commonLibDefListObserver.h"



// *****************************************************************************
// opnLibDefListObserver::opnLibDefListObserver()
// 
// Constructor which passes the priority argument to its inherited constructor.
// *****************************************************************************
opnLibDefListObserver::opnLibDefListObserver(oaUInt4    priorityIn,
                                             oaBoolean  enabledIn)
: oaObserver<oaLibDefList>(priorityIn, enabledIn)
{
}



// *****************************************************************************
// opnLibDefListObserver::onLoadWarnings()
// 
// This overloaded member function of the opnLibDefListObserver class, prints
// out either a syntax error or any warning which are issued by the oaLibDefList
// object management.
// *****************************************************************************
oaBoolean
opnLibDefListObserver::onLoadWarnings(oaLibDefList                  *list,
                                      const oaString                &msg,
                                      oaLibDefListWarningTypeEnum   type)
{
    if (type == oacNoDefaultLibDefListWarning) {
        // No warning if there is no lib.defs file found
        return true;
    } else {
        oaString libDefPath;

        if (list) {
            list->getPath(libDefPath);
        }

        cout << "ERROR: While loading the library definitions file ";
        cout << libDefPath;
        cout << ", OpenAccess generated the message:\n";
        cout << msg << endl;

        exit(1);
    }
    return true;
}



