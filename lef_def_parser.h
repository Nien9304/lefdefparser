#ifndef Lef_def_parser_H
#define Lef_def_parser_H
#include "stdio.h"
#include "oa/oaDesignDB.h"
#include "commonTechObserver.h"
#include "commonLibDefListObserver.h"
#include "commonFunctions.h"
#include <fstream>
#include <map>
#include <string.h>
using namespace oa;
static oaNativeNS ns;
struct pins
{
	string layer;
	double up_bound ;
	double down_bound ;
	double left_bound ;
	double right_bound ;
};

class OA_stdCell
{
	public:
	OA_stdCell(char *cellName,char *libName,char *libPath,char *libType);
	~OA_stdCell();
    void getLayerMap();
    void getAllCell(char *d_libName,char *libPath,char *viewType,char *outPath, 
		ostream &result_fil);
    void getInstBoundary(char *outPath);
    void getCellInf();
    typedef vector<pins*> pin_list;
    std::map<string,pin_list*> pin_list_map;
	private:
	fstream 	fout;
	oaDesign*	m_design;
	oaLib*		m_lib;
	oaTech*		m_tech;
	string m_layermap[200];

};
#endif
