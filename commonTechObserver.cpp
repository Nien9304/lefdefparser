// *****************************************************************************
// commonTechObserver.cpp
//
// This file contains the implementation of an oaTech observer for technology
// conflict notifications.
//
// This code is used in all examples to alert the users of technology conflicts.
//
// ****************************************************************************
// Except as specified in the OpenAccess terms of use of Cadence or Silicon
// Integration Initiative, this material may not be copied, modified,
// re-published, uploaded, executed, or distributed in any way, in any medium,
// in whole or in part, without prior written permission from Cadence.
//
//                Copyright 2002-2005 Cadence Design Systems, Inc.
//                           All Rights Reserved.
//
// To distribute any derivative work based upon this file you must first contact
// Si2 @ contracts@si2.org.
//
// *****************************************************************************
// *****************************************************************************


#include "commonTechObserver.h"



// *****************************************************************************
// Global Instantiation
// *****************************************************************************
oaString    opnTechConflictObserver::format
    ("ERROR: A %s conflict has been detected in the technology "
     "hierarchy. It is caused by the following list of %s: %s");

oaNativeNS  opnTechConflictObserver::ns;



// *****************************************************************************
// opnTechConflictObserver::opnTechConflictObserver()
//
// This is the default constructor for the opnTechConflictObserver class.
// *****************************************************************************
opnTechConflictObserver::opnTechConflictObserver(oaUInt4 priorityIn)
: oaObserver<oaTech>(priorityIn)
{
}



// *****************************************************************************
// opnTechConflictObserver::onConflict()
//
// This is the observer function definition for tech conflict. It handles
// conflicts by calling oaIncTechConflictTest::handleConflicts().
// *****************************************************************************
void
opnTechConflictObserver::onConflict(oaTech                  *mostDerivedTech,
                                    oaTechConflictTypeEnum  conflictType,
                                    oaObjectArray           conflictingObjs)
{
    oaString            str;
    oaString            type;
    oaString            elements;
    oaString            list;
    oaLayer             *layer;
    oaDerivedLayer      *derivedLay;
    oaPurpose           *purpose;
    oaSiteDef           *siteDef;
    oaViaDef            *viaDef;
    oaViaSpec           *viaSpec;
    oaAnalysisLib       *analysisLib;
    oaConstraint        *constraint;
    oaConstraintGroup   *constraintGroup;
    oaViaVariant        *viaVariant;

    for (oaUInt4 i = 0; i < conflictingObjs.getNumElements(); i++) {
        oaObject *obj = conflictingObjs[i];

        switch (conflictType) {
        case oacLayerNameTechConflictType:
        case oacLayerNumTechConflictType:
            layer = (oaLayer*) obj;
            list += (getObjectLibName(obj, str), str) + "/";
            list += (layer->getName(str), str);
            list += (str.format(" (#%d); ", layer->getNumber()), str);
            break;

        case oacDerivedLayerTechConflictType:
            derivedLay = (oaDerivedLayer*) obj;
            list += (getObjectLibName(obj, str), str) + "/";
            list += (derivedLay->getName(str), str) + " (";

            if (layer = derivedLay->getLayer1()) {
                list += (layer->getName(str), str) + ", ";
            } else {
                list += (str.format("#%d, ", derivedLay->getLayer1Num()), str);
            }

            if (layer = derivedLay->getLayer2()) {
                list += (layer->getName(str), str) + "); ";
            } else {
                list += (str.format("#%d); ", derivedLay->getLayer2Num()), str);
            }

            break;

        case oacPurposeNameTechConflictType:
        case oacPurposeNumTechConflictType:
            purpose = (oaPurpose*) obj;
            list += (getObjectLibName(obj, str), str) + "/";
            list += (purpose->getName(str), str);
            list += (str.format(" (#%d); ", purpose->getNumber()), str);
            break;

        case oacSiteDefNameTechConflictType:
            siteDef = (oaSiteDef*) obj;
            list += (getObjectLibName(obj, str), str) + "/";
            list += (siteDef->getName(str), str) + "; ";
            break;

        case oacViaDefNameTechConflictType:
            viaDef = (oaViaDef*) obj;
            list += (getObjectLibName(obj, str), str) + "/";
            list += (viaDef->getName(str), str) + "; ";
            break;

        case oacViaSpecTechConflictType:
            viaSpec = (oaViaSpec*) obj;
            list += (getObjectLibName(obj, str), str) + "/(";

            if (layer = viaSpec->getLayer1()) {
                list += (layer->getName(str), str) + ", ";
            } else {
                list += (str.format("#%d, ", viaSpec->getLayer1Num()), str);
            }

            if (layer = viaSpec->getLayer2()) {
                list += (layer->getName(str), str) + "); ";
            } else {
                list += (str.format("#%d); ", viaSpec->getLayer2Num()), str);
            }

            break;

        case oacAnalysisLibNameTechConflictType:
            analysisLib = (oaAnalysisLib*) obj;
            list += (getObjectLibName(obj, str), str) + "/";
            list += (analysisLib->getName(str), str) + "; ";
            break;

        case oacConstraintNameTechConflictType:
            constraint = (oaConstraint*) obj;
            list += (getObjectLibName(obj, str), str) + "/";
            list += (constraint->getName(str), str) + "; ";
            break;

        case oacConstraintGroupNameTechConflictType:
            constraintGroup = (oaConstraintGroup*) obj;
            list += (getObjectLibName(obj, str), str) + "/";
            list += (constraintGroup->getName(str), str) + "; ";
            break;

        case oacViaVariantNameTechConflictType:
        case oacStdViaVariantTechConflictType:
        case oacCustomViaVariantTechConflictType:
            viaVariant = (oaViaVariant*) obj;
            list += (getObjectLibName(obj, str), str) + "/";
            list += (viaVariant->getName(str), str) + "; ";
            break;

        default:
            break;
        }
    }

    switch (conflictType) {
    case oacLayerNameTechConflictType:
        type = "Layer Name";
        elements = "layers";
        break;

    case oacLayerNumTechConflictType:
        type = "Layer Number";
        elements = "layers";
        break;

    case oacDerivedLayerTechConflictType:
        type = "Derived Layer";
        elements = "derived layers";
        break;

    case oacPurposeNameTechConflictType:
        type = "Layer Purpose Name";
        elements = "purposes";
        break;

    case oacPurposeNumTechConflictType:
        type = "Layer Purpose Number";
        elements = "purposes";
        break;

    case oacSiteDefNameTechConflictType:
        type = "Site Definition";
        elements = "sites";
        break;

    case oacViaDefNameTechConflictType:
        type = "Via Def Name";
        elements = "via definitions";
        break;

    case oacViaSpecTechConflictType:
        type = "Via Spec";
        elements = "via specifications";
        break;

    case oacAnalysisLibNameTechConflictType:
        type = "Analysis Library Name";
        elements = "analysis libraries";
        break;

    case oacConstraintNameTechConflictType:
        type = "Constraint Name";
        elements = "constraints";
        break;

    case oacConstraintGroupNameTechConflictType:
        type = "Constraint Group Name";
        elements = "constraint groups";
        break;

    case oacViaVariantNameTechConflictType:
        type = "Via Variant Name";
        elements = "via variants";
        break;

    case oacStdViaVariantTechConflictType:
        type = "Standard Via Variant";
        elements = "standard via variants";
        break;

    case oacCustomViaVariantTechConflictType:
        type = "Custom Via Variants";
        elements = "custom via variants";
        break;

    default:
        type = "Unknown";
        elements = "Unknown";
        break;
    }

    printErrorMsg(type, elements, list);
}



// *****************************************************************************
// opnTechConflictObserver::onClearanceMeasureConflict()
//
// This function handles clearance measure conflict between tech libs.
// *****************************************************************************
void
opnTechConflictObserver::onClearanceMeasureConflict(oaTechArray cTechs)
{
    oaString    list;
    oaString    str;

    oaUInt4     i;
    for (i = 0; i < cTechs.getNumElements(); i++) {
        list += (getObjectLibName(cTechs[i], str), str) + " (";
        list += cTechs[i]->getClearanceMeasure(true).getName() + "); ";
    }

    printErrorMsg("Clearance Measure", "Libraries", list);
}



// *****************************************************************************
// opnTechConflictObserver::onDefaultManufacturingGridConflict()
//
// This function handles manufacturing grid conflicts.
// *****************************************************************************
void
opnTechConflictObserver::onDefaultManufacturingGridConflict(oaTechArray cTechs)
{
    oaString    list;
    oaString    str;

    oaUInt4     i;
    for (i = 0; i < cTechs.getNumElements(); i++) {
        list += (getObjectLibName(cTechs[i], str), str) + " (";
        str.format("%d); ", cTechs[i]->getDefaultManufacturingGrid(true));
        list += str;
    }

    printErrorMsg("Default Manufacturing Grid", "Libraries", list);
}



// *****************************************************************************
// opnTechConflictObserver::onGateGroundedConflict()
//
// This function handles conflicts between tech libs related to gate grounding.
// *****************************************************************************
void
opnTechConflictObserver::onGateGroundedConflict(oaTechArray cTechs)
{
    oaString    list;
    oaString    str;

    oaUInt4     i;
    for (i = 0; i < cTechs.getNumElements(); i++) {
        list += (getObjectLibName(cTechs[i], str), str) + " (";

        if (cTechs[i]->isGateGrounded(true)) {
            list += "grounded); ";
        } else {
            list += "not grounded); ";
        }
    }

    printErrorMsg("Gate Grounded", "Libraries", list);
}



// *****************************************************************************
// opnTechConflictObserver::onDBUPerUUConflict()
//
// This function handles conflicts between tech libs related to DBU per UU.
// *****************************************************************************
void
opnTechConflictObserver::onDBUPerUUConflict(oaTechArray cTechs,
                                            oaViewType  *viewType)
{
    oaString    list;
    oaString    viewTypeName;
    oaString    str;

    oaUInt4     i;
    for (i = 0; i < cTechs.getNumElements(); i++) {
        list += (getObjectLibName(cTechs[i], str), str) + " (";
        str.format("%d); ", cTechs[i]->getDBUPerUU(viewType, true));
        list += str;
    }

    viewType->getName(viewTypeName);
    printErrorMsg("DBU per UU (" + viewTypeName + ")", "Libraries", list);
}



// *****************************************************************************
// opnTechConflictObserver::onUserUnitsConflict()
//
// This function handles conflicts between tech libs related to user units.
// *****************************************************************************
void
opnTechConflictObserver::onUserUnitsConflict(oaTechArray    cTechs,
                                             oaViewType     *viewType)
{
    oaString    list;
    oaString    viewTypeName;
    oaString    str;

    oaUInt4     i;
    for (i = 0; i < cTechs.getNumElements(); i++) {
        list += (getObjectLibName(cTechs[i], str), str) + " (";
        list += cTechs[i]->getUserUnits(viewType, true).getName() + "); ";
    }

    viewType->getName(viewTypeName);
    printErrorMsg("User Units (" + viewTypeName + ")", "Libraries", list);
}



// *****************************************************************************
// opnTechConflictObserver::onProcessFamilyConflict()
//
// This function handles conflicting process family attributes.
// *****************************************************************************
void
opnTechConflictObserver::onProcessFamilyConflict(const oaTechArray &cTechs)
{
    oaString    list;
    oaString    str;

    oaUInt4     i;
    for (i = 0; i < cTechs.getNumElements(); i++) {
        list += (getObjectLibName(cTechs[i], str), str) + " (";
        list += (cTechs[i]->getProcessFamily(str, true), str) + "); ";
    }

    printErrorMsg("Process Family", "Libraries", list);
}



// *****************************************************************************
// opnTechConflictObserver::onExcludedLayerConflict()
//
// This function handles conflicting layer definitions.
// *****************************************************************************
void
opnTechConflictObserver::onExcludedLayerConflict(oaTech                 *tech,
                                                 const oaPhysicalLayer  *l1,
                                                 const oaLayer          *l2)
{
    oaString    str;
    oaString    list;

    list += (getObjectLibName(l1, str), str) + "/";
    list += (l1->getName(str), str) + " is mutually exclusive with ";
    list += (getObjectLibName(l2, str), str) + "/";
    list += (l2->getName(str), str) + "; ";

    printErrorMsg("Excluded Layer", "Layers", list);
}



// *****************************************************************************
// opnTechConflictObserver::getObjectLibName()
//
// This function returns the name of the library a certain object resides in.
// *****************************************************************************
void
opnTechConflictObserver::getObjectLibName(const oaObject    *obj,
                                          oaString          &str) const
{
    oaTech *tech;

    tech = (oaTech*) obj->getDatabase();

    // Return the name of the library that contains the techDB.
    // Alternatively, the full path to the library could be found as follows:
    //  tech->getLib()->getFullPath(str);
    //
    tech->getLibName(ns, str);
}



// *****************************************************************************
// opnTechConflictObserver::printErrorMsg()
//
// This function populates a format string for a tech conflict error message
// and prints it to stdout.
// *****************************************************************************
void
opnTechConflictObserver::printErrorMsg(const char   *type,
                                       const char   *elements,
                                       const char   *list) const
{
    oaString str;

    str.format(format, type, elements, list);
    cout << endl << str << endl;
}

